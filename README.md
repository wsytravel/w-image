# w-image

## 介绍

重构img标签，可以让img标签实现缩放，拖拽等操作，并返回图片缩放，偏移等数值
<br/>

## 安装

```bash
# yarn 安装
yarn add @travel_wsy/w_image

# npm 安装
npm install @travel_wsy/w_image -S
```
<br/>

## 使用方法

**1、**
组件提供的参数和方法如下：
<br/>

![Alt text](https://gitee.com/wsytravel/w-image/raw/master/image/image_1.png)
<br/>

![Alt text](https://gitee.com/wsytravel/w-image/raw/master/image/image_2.png)
<br/>
可以直接调用组件内部方法进行收到缩放和旋转
<br/>
scaleAdd：图片放大；scaleMinus：图片缩小；rotateLeft：图片逆时针旋转；rotateRight：图片顺时针旋转
<br/>
例如：
<br/>
```bash
this.$refs[''].handleActiveBtns({scaleAdd:false,scaleMinus:false,rotateLeft:false,rotateRight:false})
```
<br/>

**2、**
引入方法

```bash
import {WImage} from '@travel_wsy/w_image'
```
<br/>